library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity alu is
	generic (
		C_data_width: integer := 4
	);	port (
	A, B: in std_logic_vector(3 downto 0);
	aluOp: in std_logic_vector(2 downto 0);
	Z: out std_logic_vector(3 downto 0)
	);
end alu;

architecture x of alu is 
begin
		
with aluOp select
	Z <= 	shr(A, B) when "000",
		((A * B)((C_data_width-1) downto 0)) when "001",
		(A and B) when "010",
		(not(A or B)) when "011",
		(A + B) when "100",
		(A or B) when "101",
		(A - B) when "110",
		(A xor B) when "111";

end;
